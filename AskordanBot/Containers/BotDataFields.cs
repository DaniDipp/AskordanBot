﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskordanBot.Containers
{
    public static class Responses
    {
        public static string Start = $"*Hello!*{Environment.NewLine}This is the introductory statement of the bot.";
        public static string ShuttingDown = "Shutting down.";
        public static string FeedbackTitle = $"Please send me your Feedback, I will forward it to the developers.{Environment.NewLine}Use /cancel to abort.";
        public static string FeedbackCancelled = "Feedback cancelled.";
        public static string FeedbackCompleted = "Thank you for your Feedback! We will do our best to improve this bot in the future.";
        public static string NotImplemented = "This feature is currently in development.";
    }

    /// <summary>
    /// Defines a User the Bot is in contact with.
    /// </summary>
    /// <param type="User" name="TelegramUser">Telegram information of the user.</param>
    public class BotUser
    {
        public User TelegramUser { get; set; } //id, firstName, *lastName, *userName
        public string WaitingForFeedback { get; set; } = "";

        public BotUser(User telegramUser) { TelegramUser = telegramUser; }
    }
}
