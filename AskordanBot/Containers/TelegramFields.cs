﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace AskordanBot.Containers
{
    /// <summary>
    /// You get thís back from getUpdates
    /// </summary>
    public class UpdateResponse
    {
        public bool Ok { get; set; }
        public Update[] Result { get; set; }

        public UpdateResponse(bool ok, Update[] result) { Ok = ok; Result = result; }
    }

    /// <summary>
    /// You get this back from sendMessage
    /// </summary>
    public class MessageResponse
    {
        public bool Ok { get; set; }
        public Message Result { get; set; }

        public MessageResponse(bool ok, Message result) { Ok = ok; Result = result; }
    }

    public class CallbackQueryResponse
    {
        public bool Ok { get; set; }
        public bool Result { get; set; }

        public CallbackQueryResponse(bool ok, bool result) { Ok = ok; Result = result; }
    }

    /// <summary>
    /// This object represents an incoming update.
    /// At most one of the optional parameters can be present in any given update.
    /// </summary>
    /// <param name="UpdateType">Defines which of the optional parameters are present in any given update.</param>
    /// <param name="Update_id">The update‘s unique identifier. Update identifiers start from a certain positive number and increase sequentially.</param>
    /// <param name="Message">New incoming message of any kind — text, photo, sticker, etc.</param>
    /// <param name="Edited_message">New version of a message that is known to the bot and was edited</param>
    /// <param name="Channel_post">New incoming channel post of any kind — text, photo, sticker, etc.</param>
    /// <param name="Edited_channel_post">New version of a channel post that is known to the bot and was edited</param>
    /// <param name="Inline_query">New incoming inline query</param>
    /// <param name="Chosen_inline_result">The result of an inline query that was chosen by a user and sent to their chat partner.</param>
    /// <param name="Callback_query">New incoming callback query</param>
    /// <param name="Shipping_uery">New incoming shipping query. Only for invoices with flexible price</param>
    /// <param name="Pre_checkout_query">New incoming pre-checkout query. Contains full information about checkout</param>
    public class Update
    {
        public int Update_id { get; set; }
        public Message Message { get; set; } = null;
        public Message Edited_message { get; set; } = null;
        public Message Channel_post { get; set; } = null;
        public Message Edited_channel_post { get; set; } = null;
        public InlineQuery Inline_query { get; set; } = null;
        public ChosenInlineResult Chosen_inline_result { get; set; } = null;
        public CallbackQuery Callback_query { get; set; } = null;
        public ShippingQuery Shipping_query { get; set; } = null;
        public PreCheckoutQuery Pre_checkout_query { get; set; } = null;
        public UpdateTypes UpdateType { get; set; }

        public enum UpdateTypes
        {
            message, edited_message, channel_post, edited_channel_post, inline_query, chosen_inline_result, callback_query
        }

        public Update(int update_id) { Update_id = update_id; }
    }

    public class WebhookInfo
    {
        public string Url { get; set; }
        public bool Has_custom_certificate { get; set; }
        public int Pending_update_count { get; set; }
        public int Last_error_date { get; set; } = 0;
        public string Last_error_message { get; set; } = string.Empty;

        public WebhookInfo(string url, bool has_custom_certificate, int pending_update_count) { Url = url; Has_custom_certificate = has_custom_certificate; Pending_update_count = pending_update_count; }
    }

    public class User
    {
        public int Id { get; set; }
        public string First_name { get; set; }
        public string Last_name { get; set; } = string.Empty;
        public string Username { get; set; } = string.Empty;

        public User(int id, string first_name) { Id = id; First_name = first_name; }

    }

    public class Chat
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public string Title { get; set; } = string.Empty;
        public string First_name { get; set; } = string.Empty;
        public string Last_name { get; set; } = string.Empty;
        public bool All_members_are_administrators { get; set; } = true;

        public Chat(long id, string type) { Id = id; Type = type; }
    }

    public class Message
    {
        public int Message_id { get; set; }
        public User From { get; set; } = null;
        public int Date { get; set; }
        public Chat Chat { get; set; }
        public User Forward_from { get; set; } = null;
        public Chat Forward_from_chat { get; set; } = null;
        public int Forward_from_message_id { get; set; } = 0;
        public int Forward_date { get; set; } = 0;
        public Message Reply_to_message { get; set; } = null;
        public int Edit_date { get; set; } = 0;
        public string Text { get; set; } = string.Empty;
        public MessageEntity[] Entities { get; set; } = null;
        public Audio Audio { get; set; } = null;
        public Document Document { get; set; } = null;
        public Game Game { get; set; } = null;
        public PhotoSize[] Photo { get; set; } = null;
        public Sticker Sticker { get; set; } = null;
        public Video Video { get; set; } = null;
        public Voice Voice { get; set; } = null;
        public string Caption { get; set; } = string.Empty;
        public Contact Contact { get; set; } = null;
        public Location Location { get; set; } = null;
        public Venue Venue { get; set; } = null;
        public User New_chat_memeber { get; set; } = null;
        public User Left_chat_member { get; set; } = null;
        public string New_chat_title { get; set; } = string.Empty;
        public PhotoSize[] New_chat_photo { get; set; } = null;
        public bool Delete_chat_photo { get; set; } = true;
        public bool Group_chat_created { get; set; } = true;
        public bool Supergroup_chat_created { get; set; } = true;
        public bool Channel_chat_created { get; set; } = true;
        public int Migrate_to_chat_id { get; set; } = 0;
        public int Migrate_from_chat_id { get; set; } = 0;
        public Message Pinned_message { get; set; } = null;

        public Message(int message_id, int date, Chat chat) { Message_id = message_id; Date = date; Chat = chat; }
    }

    public class MessageEntity
    {
        public string Type { get; set; }
        public int Offset { get; set; }
        public int Length { get; set; }
        public string Url { get; set; } = string.Empty;
        public User User { get; set; } = null;

        public MessageEntity(string type, int offset, int length) { Type = type; Offset = offset; Length = length; }
    }

    public class PhotoSize
    {
        public string File_id { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int File_size { get; set; } = 0;

        public PhotoSize(string file_id, int width, int height) { File_id = file_id; Width = width; Height = height; }
    }

    public class Audio
    {
        public string File_id { get; set; }
        public int Duration { get; set; }
        public string Performer { get; set; } = string.Empty;
        public string Title { get; set; } = string.Empty;
        public string Mime_type { get; set; } = string.Empty;
        public int File_size { get; set; } = 0;

        public Audio(string file_id, int duration) { File_id = file_id; Duration = duration; }
    }

    public class Document
    {
        public string File_id { get; set; }
        public PhotoSize Thumb { get; set; } = null;
        public string File_name { get; set; } = string.Empty;
        public string Mime_type { get; set; } = string.Empty;
        public int File_size { get; set; } = 0;

        public Document(string file_id) { File_id = file_id; }
    }

    public class Sticker
    {
        public string File_id { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public PhotoSize Thumb { get; set; } = null;
        public string Emoji { get; set; } = string.Empty;
        public int File_size { get; set; } = 0;

        public Sticker(string file_id, int width, int height) { File_id = file_id; Width = width; Height = height; }
    }

    public class Video
    {
        public string File_id { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Duration { get; set; }
        public PhotoSize Thumb { get; set; } = null;
        public string Mime_type { get; set; } = string.Empty;
        public int File_size { get; set; } = 0;

        public Video(string file_id, int width, int height) { File_id = file_id; Width = width; Height = height; }
    }

    public class Voice
    {
        public string File_id { get; set; }
        public int Duration { get; set; }
        public string Mime_type { get; set; } = string.Empty;
        public int File_size { get; set; } = 0;

        public Voice(string file_id, int duration) { File_id = file_id; Duration = duration; }
    }

    public class Contact
    {
        public string Phone_number { get; set; }
        public string First_name { get; set; }
        public string Last_name { get; set; } = string.Empty;
        public int User_id { get; set; } = 0;

        public Contact(string phone_number, string first_name) { Phone_number = phone_number; First_name = first_name; }
    }

    public class Location
    {
        public float Longitude { get; set; }
        public float Latitude { get; set; }

        public Location(float longitude, float latitude) { Longitude = longitude; Latitude = latitude; }
    }

    public class Venue
    {
        public Location Location { get; set; }
        public string Title { get; set; }
        public string Address { get; set; }
        public string Foursquare_id { get; set; } = string.Empty;

        public Venue(Location location, string title, string address) { Location = location; Title = title; Address = address; }
    }

    public class UserProfilePhotos
    {
        public int Total_count { get; set; }
        public PhotoSize[][] Photos { get; set; }

        public UserProfilePhotos(int total_count, PhotoSize[][] photos) { Total_count = total_count; Photos = photos; }
    }

    public class File
    {
        public string File_id { get; set; }
        public int File_size { get; set; } = 0;
        public string File_path { get; set; } = string.Empty;

        public File(string file_id) { File_id = file_id; }
    }

    public class ReplyKeyboardMarkup
    {
        public KeyboardButton[][] keyboard { get; set; }
        public bool resize_keyboard { get; set; } = false;
        public bool one_time_keyboard { get; set; } = false;
        public bool selective { get; set; } = false;

        public ReplyKeyboardMarkup(KeyboardButton[][] _keyboard) { keyboard = _keyboard; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public class KeyboardButton
    {
        public string text { get; set; }
        public bool request_contact { get; set; } = false;
        public bool request_location { get; set; } = false;

        public KeyboardButton(string _text) { text = _text; }
    }

    public class ReplyKeyboardRemove
    {
        public bool remove_keyboard { get; set; } = true;
        public bool Sselective { get; set; } = false;

        public ReplyKeyboardRemove() { }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public class InlineKeyboardMarkup
    {
        public InlineKeyboardButton[][] inline_keyboard { get; set; }

        public InlineKeyboardMarkup(InlineKeyboardButton[][] _inline_keyboard) { inline_keyboard = _inline_keyboard; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this); //TODO: Convert all Field names to lower case...
        }
    }

    public class InlineKeyboardButton
    {
        public string text { get; set; }
        public string url { get; set; } = string.Empty;
        public string callback_data { get; set; } = string.Empty;
        public string switch_inline_query { get; set; } = string.Empty;
        public string switch_inline_query_current_chat { get; set; } = string.Empty;
        public CallbackGame callback_game { get; set; } = null;

        public InlineKeyboardButton(string _text, string _url = "", string _callback_data = "") { text = _text; url = _url; callback_data = _callback_data; }
    }

    public class CallbackQuery
    {
        public string id { get; set; }
        public User from { get; set; }
        public Message message { get; set; } = null;
        public string inline_message_id { get; set; } = string.Empty;
        public string chat_instance { get; set; } = string.Empty;
        public string data { get; set; } = string.Empty;
        public string game_short_name { get; set; } = string.Empty;

        public CallbackQuery(string _id, User _from) { id = _id; from = _from; }
    }

    public class ForceReply
    {
        public bool force_reply { get; set; } = true;
        public bool selective { get; set; } = false;

        public ForceReply() { }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public class ChatMember
    {
        public User User { get; set; }
        public string Status { get; set; }

        public ChatMember(User user, string status) { User = user; Status = status; }
    }

    public class ResponseParameters
    {
        public int Migrate_to_chat_id { get; set; } = 0;
        public int Retry_after { get; set; } = 0;

        public ResponseParameters() { }
    }

    public class InputFile
    {
        //This object represents the contents of a file to be uploaded. Must be posted using multipart/form-data in the usual way that files are uploaded via the browser.
    }

    public class InlineQuery
    {
        public string Id { get; set; }
        public User From { get; set; }
        public Location Location { get; set; } = null;
        public string Query { get; set; }
        public string Offset { get; set; }

        public InlineQuery(string id, User from, string query, string offset) { Id = id; From = from; Query = query; Offset = offset; }
    }

    public class InlineQueryResult
    {
        //Yeah, I'm not going to figure this shit out
    }

    public class ChosenInlineResult
    {
        public string Result_id { get; set; }
        public User From { get; set; }
        public Location Location { get; set; } = null;
        public string Inline_message_id { get; set; }
        public string Query { get; set; }

        public ChosenInlineResult(string result_id, User from, string inline_message_id, string query) { Result_id = result_id; From = from; Inline_message_id = inline_message_id; Query = query; }
    }

    ///Payments
    /// Your bot can accept payments from Telegram users. Please see the introduction to payments (https://core.telegram.org/bots/payments) for more details on the process and how to set up payments for your bot.
    /// Please note that users will need Telegram v.4.0 or higher to use payments (released on May 18, 2017).

    /// <summary>
    /// This object represents a portion of the price for goods or services.
    /// </summary>
    /// <param name="label">Portion label</param>
    /// <param name="amount">Price of the product in the smallest units of the currency (https://core.telegram.org/bots/payments#supported-currencies) (integer, not float/double). For example, for a price of US$ 1.45 pass amount = 145. See the exp parameter in currencies.json (https://core.telegram.org/bots/payments/currencies.json), it shows the number of digits past the decimal point for each currency (2 for the majority of currencies).</param>
    public class LabeledPrice
    {
        public string Label { get; set; }
        public int Amount { get; set; }

        public LabeledPrice(string label, int amount) { Label = label; Amount = amount; }
    }

    /// <summary>
    /// This object contains basic information about an invoice.
    /// </summary>
    /// <param name="Title">Product name</param>
    /// <param name="Description">Product descriotion</param>
    /// <param name="Start_parameter">Unique bot deep-linking parameter that can be used to generate this invoice</param>
    /// <param name="Currency">Three-letter ISO 4217 currency code</param>
    /// <param name="Total_amount">Total price in the smallest units of the currency (integer, not float/double). For example, for a price of US$ 1.45 pass amount = 145. See the exp parameter in currencies.json (https://core.telegram.org/bots/payments#supported-currencies), it shows the number of digits past the decimal point for each currency (2 for the majority of currencies).</param>
    public class Invoice
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Start_parameter { get; set; }
        public string Currency { get; set; }
        public int Total_amount { get; set; }

        public Invoice(string title, string description, string start_parameter, string currency, int total_amount) { Title = title; Description = description; Start_parameter = start_parameter; Currency = currency; Total_amount = total_amount; }
    }

    public class ShippingAddress
    {
        public string Country_code { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Street_line1 { get; set; }
        public string Street_line2 { get; set; }
        public string Post_code { get; set; }

        public ShippingAddress(string country_code, string state, string city, string street_line1, string street_line2, string post_code) { Country_code = country_code; State = state; City = city; Street_line1 = street_line1; Street_line2 = street_line2; }
    }

    public class OrderInfo
    {
        public string Name { get; set; } = null;
        public string Phone_number { get; set; } = null;
        public string Email { get; set; } = null;
        public ShippingAddress Shipping_address { get; set; } = null;

        public OrderInfo() { }
        public OrderInfo(string name, string phone_number, string email, ShippingAddress shipping_address) { Name = name; Phone_number = phone_number; Email = email; Shipping_address = shipping_address; }
    }

    public class ShippingOption
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public LabeledPrice[] Prices { get; set; }

        public ShippingOption(string id, string title, LabeledPrice[] prices) { Id = id; Title = title; Prices = prices; }
    }

    public class SuccessfulPayment
    {
        public string Currency { get; set; }
        public int Total_amount { get; set; }
        public string Invoice_payload { get; set; }
        public string Shipping_option_id { get; set; } = null;
        public OrderInfo Order_info { get; set; } = null;
        public string Telegram_payment_charge_id { get; set; }
        public string Provider_payment_charge_id { get; set; }

        public SuccessfulPayment(string currency, int total_amount, string invoice_payload, string telegram_payment_charge_id, string provider_payment_charge_id) { Currency = currency; Total_amount = total_amount; Invoice_payload = invoice_payload; Telegram_payment_charge_id = telegram_payment_charge_id; Provider_payment_charge_id = provider_payment_charge_id; }
        public SuccessfulPayment(string currency, int total_amount, string invoice_payload, string shipping_option_id, OrderInfo order_info, string telegram_payment_charge_id, string provider_payment_charge_id) { Currency = currency; Total_amount = total_amount; Invoice_payload = invoice_payload; Shipping_option_id = shipping_option_id; Order_info = order_info; Telegram_payment_charge_id = telegram_payment_charge_id; Provider_payment_charge_id = provider_payment_charge_id; }
    }

    public class ShippingQuery
    {
        public string Id { get; set; }
        public User From { get; set; }
        public string Invoice_payload { get; set; }
        public ShippingAddress Shipping_address { get; set; }

        public ShippingQuery(string id, User from, string invoice_payload, ShippingAddress shipping_address) { Id = id; From = from; Invoice_payload = invoice_payload; Shipping_address = shipping_address; }
    }

    public class PreCheckoutQuery
    {
        public string Id { get; set; }
        public User From { get; set; }
        public string Currency { get; set; }
        public int TotalAmount { get; set; }
        public string Invoice_payload { get; set; }
        public string Shipping_option_id { get; set; } = null;
        public OrderInfo Order_info { get; set; } = null;
        
        public PreCheckoutQuery(string id, User from, string currency, int total_amount, string invoice_payload) { Id = id; From = from; Currency = currency; TotalAmount = total_amount; Invoice_payload = invoice_payload; }
        public PreCheckoutQuery(string id, User from, string currency, int total_amount, string invoice_payload, string shipping_option_id, OrderInfo order_info) { Id = id; From = from; Currency = currency; TotalAmount = total_amount; Invoice_payload = invoice_payload; Shipping_option_id = shipping_option_id; Order_info = order_info; }
    }

    //Games
    public class Game
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public PhotoSize[] Photo { get; set; }
        public string Text { get; set; } = string.Empty;
        public MessageEntity[] Text_entities { get; set; } = null;
        public Animation Animation { get; set; } = null;

        public Game(string title, string description, PhotoSize[] photo) { Title = title; Description = description; Photo = photo; }
    }

    public class Animation
    {
        public string File_id { get; set; }
        public PhotoSize Thumb { get; set; } = null;
        public string File_name { get; set; } = string.Empty;
        public string Mime_type { get; set; } = string.Empty;
        public int File_site { get; set; } = 0;

        public Animation(string file_id) { File_id = file_id; }
    }

    public class CallbackGame
    {
        //A placeholder, currently holds no information. Use BotFather to set up your game.
    }

    public class GameHighScore
    {
        public int Position { get; set; }
        public User User { get; set; }
        public int Score { get; set; }

        public GameHighScore(int position, User user, int score) { Position = position; User = user; Score = score; }
    }
}
