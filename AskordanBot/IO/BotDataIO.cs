﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AskordanBot.Containers;
using AskordanBot.Telegram;
using Newtonsoft.Json;

namespace AskordanBot.IO
{
    public class BotDataIO
    {
        /// <summary>
        /// Saves current user data at Resources/UserData.json
        /// </summary>
        /// <returns>true, if saving was successful</returns>
        public static bool SaveUserData()
        {
            string json = JsonConvert.SerializeObject(TelegramBot.BotUsers);
            try
            {
                System.IO.File.WriteAllText($@"Resources/user-data.json", json);
            }
            catch (Exception e)
            {
                if (e.GetType().IsAssignableFrom(typeof(System.IO.DirectoryNotFoundException)))
                {
                    System.IO.Directory.CreateDirectory(@"Resources");
                    SaveUserData();
                }
                else Console.WriteLine("Exception: " + e.ToString());
            }
            return true;
        }

        /// <summary>
        /// Loads UserData from Resources/UserData.json
        /// </summary>
        /// <returns>Array of all BotUsers</returns>
        public static BotUser[] LoadUserData()
        {
            string json = System.IO.File.ReadAllText($@"Resources/user-data.json");
            BotUser[] botUsers = JsonConvert.DeserializeObject<BotUser[]>(json);
            return botUsers;
        }

        /// <summary>
        /// This class checks the given User against the given User-Database and adds the User, if he doesn't exist.
        /// </summary>
        /// <param name="tUser">User to check</param>
        /// <returns>Complete User-Database</returns>
        public static BotUser[] CheckFirstTimeUser(User tUser)
        {
            bool existing = false;
            if (TelegramBot.BotUsers != null)
            {
                for (int i = 0; i < TelegramBot.BotUsers.Length; i++)
                {
                    if (TelegramBot.BotUsers[i].TelegramUser.Id == tUser.Id)
                    {
                        existing = true;
                        break;
                    }
                }
            }
            BotUser[] allBotUsers = null;

            if (!existing)
            {
                allBotUsers = new BotUser[TelegramBot.BotUsers?.Length ?? 0 + 1];
                for (int i = 0; i < allBotUsers.Length - 1; i++)
                {
                    allBotUsers[i] = TelegramBot.BotUsers[i];
                }
                allBotUsers[allBotUsers.Length - 1] = new BotUser(tUser);

                Console.WriteLine("Adding new User: " + tUser.First_name + " @" + tUser.Username + " " + tUser.Last_name);
                SaveUserData();
            }
            return allBotUsers ?? TelegramBot.BotUsers;

        }

        public static BotUser GetBotUser(User user)
        {
            foreach (BotUser botUser in TelegramBot.BotUsers)
            {
                if (botUser.TelegramUser.Id == user.Id) return botUser;
            }
            return null;
        }

        public static string GetUserName(User user)
        {
            BotUser botUser = GetBotUser(user);
            
            if (!string.IsNullOrWhiteSpace(user.Username)) return '@' + user.Username;
            return user.First_name + (string.IsNullOrEmpty(user.Last_name) ? "" : ' ' + user.Last_name);
        }

        public static BotUser ClearFeedback(BotUser botUser)
        {
            botUser.WaitingForFeedback = "";
            return botUser;
        }

        public static User GetUserById(int targetId)
        {
            for (int i = 0; i < TelegramBot.BotUsers.Length; i++)
            {
                if (TelegramBot.BotUsers[i].TelegramUser.Id == targetId) return TelegramBot.BotUsers[i].TelegramUser;
            }
            return null;
        }

        public static void InsertBotUser(BotUser botUser)
        {
            for (int i = 0; i < TelegramBot.BotUsers.Length; i++)
            {
                if (TelegramBot.BotUsers[i].TelegramUser.Id == botUser.TelegramUser.Id)
                {
                    TelegramBot.BotUsers[i] = botUser;
                    SaveUserData();
                    break;
                }
            }
        }



    }
}
