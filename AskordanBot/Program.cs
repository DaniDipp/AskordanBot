﻿using AskordanBot.Telegram;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskordanBot
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bot starting up...");
            TelegramBotHandler.Start();
        }
    }
}
