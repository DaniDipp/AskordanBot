﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AskordanBot.Containers;
using AskordanBot.IO;
using Newtonsoft.Json;
using System.Net.Http;

namespace AskordanBot.Telegram
{
    public class TelegramBot
    {
        //Telegram Interaction
        private const string API_TOKEN = "413417585:AAEJ8DoRz60cTFrEV5QcpMGXywGl4dBt3kg"; //API-Token from your Telegram Bot. You get it from @Botfather.
        private const string BASE_URL = "https://api.telegram.org/bot"; //API-URL for Telegram Bots
        private long currentOffset = 0; //Current Update-Offset. Starts at 0 to get all Updates since the Bot was last online.

        //Bot-Constants
        public const string VERSION_NUMBER = "0.1";

        //Bot-Variables
        public static BotUser[] BotUsers;
        public bool ReadyToShutdown = false; //Indicates if the Bot will shutdown with the next update-cycle.


        public TelegramBot()
        {

        }

        public void Update()
        {
            Update[] updates = GetUpdates(currentOffset + 1);

            if (updates == null || updates.Length <= 0)
            {
                if (ReadyToShutdown)
                {
                    Console.WriteLine("Stopping Hoermsch");
                    //Save stuff if applicable
                    BotDataIO.SaveUserData();
                    TelegramBotHandler.Stop();
                }
            }
            else if (updates != null && updates.Length > 0)
            {
                Console.WriteLine("New Update!");
                foreach (Update update in updates)
                {
                    currentOffset = update.Update_id;
                    switch (update.UpdateType)
                    {
                        //User sent message
                        case Containers.Update.UpdateTypes.message:
                            BotUsers = BotDataIO.CheckFirstTimeUser(update.Message.From);
                            messageRecieved(update.Message);
                            break;

                        //User edited message
                        //case Containers.Update.UpdateTypes.edited_message:
                        //    BotUsers = BotDataIO.CheckFirstTimeUser(update.Edited_message.From, BotUsers);
                        //    messageEdited(update.Message);
                        //    break;

                        //User pressed a Keyboard button
                        case Containers.Update.UpdateTypes.callback_query:
                            BotUsers = BotDataIO.CheckFirstTimeUser(update.Callback_query.from);
                            callbackQuery(update.Callback_query);
                            break;

                        default:
                            //The bot doesn't support this feature
                            Console.WriteLine("Bot can't handle this type of update!");
                            break;
                    }
                }
            }
        }

        private void messageRecieved(Message message)
        {
            string name = BotDataIO.GetUserName(message.From);
            Console.WriteLine($"[Message] {name}: {message.Text}");
            if (!string.IsNullOrWhiteSpace(BotDataIO.GetBotUser(message.From).WaitingForFeedback))
            {
                switch (BotDataIO.GetBotUser(message.From).WaitingForFeedback)
                {
                    case "feedback":
                        switch (message.Text)
                        {
                            case "/cancel":
                                SendMessage(message.From.Id, Responses.FeedbackCancelled);
                                break;

                            default:
                                //Handle Feedback
                                SendMessage(message.From.Id, Responses.FeedbackCompleted);
                                break;
                        }
                        break;
                    default:
                        Console.WriteLine("[ERROR] Feedback-type not handled");
                        break;
                }
                BotDataIO.InsertBotUser(BotDataIO.ClearFeedback(BotDataIO.GetBotUser(message.From)));
            }
            else
            {
                switch (message.Text)
                {
                    //Define commands here
                    case "/start":
                        SendMessage(message.From.Id, Responses.Start);
                        Console.WriteLine("Responded: " + Responses.Start);
                        break;

                    case "/feedback":
                        SendMessage(message.From.Id, Responses.FeedbackTitle);
                        BotUser updatedBotUser = BotDataIO.GetBotUser(message.From);
                        updatedBotUser.WaitingForFeedback = "feedback";
                        BotDataIO.InsertBotUser(updatedBotUser);
                        break;

                    case "/stop":
                        //if (BotDataIO.GetBotUser(message.From).IsAdmin)
                        //{
                            SendMessage(message.From.Id, Responses.ShuttingDown);
                            Console.WriteLine("Recieved shutdown command. Getting ready to shutdown.");
                            ReadyToShutdown = true;
                        //}
                        break;

                    default:
                        Console.WriteLine("[Warning] Didn't repond.");
                        break;
                }
            }
        }

        private void messageEdited(Message editedMessage)
        {

        }

        private void callbackQuery(CallbackQuery callback)
        {
            string name = BotDataIO.GetUserName(callback.from);
            Console.WriteLine($"[Callback] {name}: {callback.data}");
            switch (callback.data)
            {
                //case "showMainMenu":
                //    EditMessageText(callback.from.Id, callback.message.Message_id, "", Responses.MainMenuTitle, "Markdown", false, MenuIO.GetMenuMarkup());
                //    AnswerCallbackQuery(callback.id);
                //    break;

                default:
                    AnswerCallbackQuery(callback.id, Responses.NotImplemented); //Error: Callback Query (" + callback.data + ") not recognized." + Environment.NewLine + "Please contact an administrator.
                    break;
            }
        }

        /// <summary>
        /// Asks for pending messages.
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="timeout"></param>
        /// <returns>An Update[] if everything is ok, null otherwise.</returns>
        public Update[] GetUpdates(long offset = 0, int limit = 100, int timeout = 0)
        {
            string url = $"{BASE_URL}{API_TOKEN}/getUpdates?offset={offset}&limit={limit}&timeout={timeout}";
            string _response = string.Empty;
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    _response = client.GetStringAsync(url).Result;
                }
                catch (Exception e)
                {
                    //Console.WriteLine(e.StackTrace);
                    Console.WriteLine("Exception: " + e.Message);
                    //jo_mai
                }
            }
            if (!string.IsNullOrWhiteSpace(_response))
            {
                UpdateResponse response = null;
                try
                {
                    response = JsonConvert.DeserializeObject<UpdateResponse>(_response) as UpdateResponse;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    //jo_mai
                }
                //If Not Ok Then Return Not OK As Exception
                if (response != null && response.Ok)
                {
                    Update[] updates = new Update[response.Result.Length];
                    for (int i = 0; i < response.Result.Length; i++)
                    {
                        Update update = response.Result[i];

                        if (update.Message != null) update.UpdateType = Containers.Update.UpdateTypes.message;
                        else if (update.Edited_message != null) update.UpdateType = Containers.Update.UpdateTypes.edited_message;
                        else if (update.Channel_post != null) update.UpdateType = Containers.Update.UpdateTypes.channel_post;
                        else if (update.Edited_channel_post != null) update.UpdateType = Containers.Update.UpdateTypes.edited_channel_post;
                        else if (update.Inline_query != null) update.UpdateType = Containers.Update.UpdateTypes.inline_query;
                        else if (update.Chosen_inline_result != null) update.UpdateType = Containers.Update.UpdateTypes.chosen_inline_result;
                        else if (update.Callback_query != null) update.UpdateType = Containers.Update.UpdateTypes.callback_query;

                        updates[i] = update;
                    }

                    return updates;
                }
            }
            return null;
        }

        public User GetMe()
        {
            return null;
        }

        public void SetWebhook(string url = null, InputFile certificate = null)
        {

        }

        public WebhookInfo GetWebhookInfo()
        {
            return null;
        }

        //replyMarkup: InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply
        public Message SendMessage(long chat_id, string text, string parse_mode = "Markdown", bool disable_web_page_preview = false, bool disable_notification = false, int reply_to_message_id = 0, string reply_markup = null)
        {
            string url = $"{BASE_URL}{API_TOKEN}/sendMessage?chat_id={chat_id}&text={text}&parse_mode={parse_mode}&disable_web_page_preview={disable_web_page_preview}&disable_notification={disable_notification}&reply_to_message_id={reply_to_message_id}&reply_markup={reply_markup}";
            string _response = string.Empty;
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    _response = client.GetStringAsync(url).Result;
                }
                catch (Exception e)
                {
                    //Console.WriteLine(e.StackTrace);
                    Console.WriteLine("Exception: " + e.Message);
                    //jo_mai
                }
            }
            if (!string.IsNullOrWhiteSpace(_response))
            {
                MessageResponse response = null;
                try
                {
                    response = JsonConvert.DeserializeObject<MessageResponse>(_response) as MessageResponse;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    //jo_mai
                }

                //If Not Ok Then Return Not OK As Exception
                if (response != null && response.Ok)
                {
                    return response.Result;
                }
            }
            return null;
        }

        public Message ForwardMessage(long chat_id, int from_chat_id, int message_id, bool disable_notification = false)
        {
            string url = $"{BASE_URL}{API_TOKEN}/forwardMessage?chat_id={chat_id}&from_chat_id={from_chat_id}&message_id={message_id}&disable_notification={disable_notification}";
            string _response = string.Empty;
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    _response = client.GetStringAsync(url).Result;
                }
                catch (Exception e)
                {
                    //Console.WriteLine(e.StackTrace);
                    Console.WriteLine("Exception: " + e.Message);
                    //jo_mai
                }
            }
            if (!string.IsNullOrWhiteSpace(_response))
            {
                MessageResponse response = null;
                try
                {
                    response = JsonConvert.DeserializeObject<MessageResponse>(_response) as MessageResponse;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    //jo_mai
                }

                //If Not Ok Then Return Not OK As Exception
                if (response != null && response.Ok)
                {
                    return response.Result;
                }
            }
            return null;
        }

        //replyMarkup: InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply
        public Message SendPhoto(int chat_id, InputFile photo, string caption = null, bool disable_notification = false, int reply_to_message_id = 0, string reply_markup = null)
        {
            return null;
        }

        //replyMarkup: InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply
        public Message SendAudio(int chat_id, InputFile audio, string caption = null, int duration = 0, string performer = null, string title = null, bool disable_notification = false, int reply_to_message_id = 0, string reply_markup = null)
        {
            return null;
        }

        //replyMarkup: InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply
        public Message SendDocument(int chat_id, InputFile document, string caption = null, bool disable_notification = false, int reply_to_message_id = 0, string reply_markup = null)
        {
            return null;
        }

        //replyMarkup: InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply
        public Message SendSticker(int chat_id, InputFile sticker, bool disable_notification = false, int reply_to_message_id = 0, string reply_markup = null)
        {
            return null;
        }

        //replyMarkup: InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply
        public Message SendVideo(int chat_id, InputFile video, int duration = 0, int width = 0, int height = 0, string caption = null, bool disable_notification = false, int reply_to_message_id = 0, string reply_markup = null)
        {
            return null;
        }

        //replyMarkup: InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply
        public Message SendVoice(int chat_id, InputFile voice, string caption = null, int duration = 0, bool disable_notification = false, int reply_to_message_id = 0, string reply_markup = null)
        {
            return null;
        }

        //replyMarkup: InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply
        public Message SendLocation(int chat_id, float latitude, float longitude, bool disable_notification = false, int reply_to_message_id = 0, string reply_markup = null)
        {
            return null;
        }

        //replyMarkup: InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply
        public Message SendVenue(int chat_id, float latitude, float longitude, string title, string address, string foursquare_id = null, bool disable_notification = false, int reply_to_message_id = 0, string reply_markup = null)
        {
            return null;
        }

        //replyMarkup: InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply
        public Message SendContact(int chat_id, string phone_number, string first_name, string last_name = null, bool disable_notification = false, int reply_to_message = 0, string reply_markup = null)
        {
            return null;
        }

        public bool SendChatAction(int chat_id, string action)
        {
            return true;
        }

        public UserProfilePhotos GetUserProfilePhotos(int user_id, int offset = 0, int limit = 100)
        {
            return null;
        }

        public File GetFile(string file_id)
        {
            return null;
        }

        public bool KickChatMember(int chat_id, int user_id)
        {
            return true;
        }

        public bool LeaveChat(int chat_id)
        {
            return true;
        }

        public bool UnbanChatMember(int chat_id, int user_id)
        {
            return true;
        }

        public Chat GetChat(int chat_id)
        {
            return null;
        }

        public ChatMember[] GetChatAdministrators(int chat_id)
        {
            return null;
        }

        public int GetChatMembersCound(int chat_id)
        {
            return 0;
        }

        public ChatMember GetChatMember(int chat_id, int user_id)
        {
            return null;
        }

        public bool AnswerCallbackQuery(string callback_query_id, string text = null, bool show_alert = false, string url = null, int cache_time = 0)
        {
            string newUrl = $"{BASE_URL}{API_TOKEN}/answerCallbackQuery?callback_query_id={callback_query_id}&text={text}&show_alert={show_alert}&url={url}&cache_time={cache_time}";
            string _response = string.Empty;
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    _response = client.GetStringAsync(newUrl).Result;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception: " + e.Message);
                    //jo_mai
                }
            }
            if (!string.IsNullOrWhiteSpace(_response))
            {
                CallbackQueryResponse response = null;
                try
                {
                    response = JsonConvert.DeserializeObject<CallbackQueryResponse>(_response) as CallbackQueryResponse;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    //jo_mai
                }
                //If Not Ok Then Return Not OK As Exception
                if (response != null && response.Ok)
                {
                    return true;
                }
            }
            return false;
        }

        public Message EditMessageText(int chat_id = 0, int message_id = 0, string inline_message_id = "", string text = "", string parse_mode = "Markdown", bool disable_web_page_preview = false, string reply_markup = "")
        {
            string url = $"{BASE_URL}{API_TOKEN}/editMessageText?chat_id={chat_id}&message_id={message_id}&inline_message_id={inline_message_id}&text={text}&parse_mode={parse_mode}&disable_web_page_preview={disable_web_page_preview}&reply_markup={reply_markup}";
            string _response = string.Empty;
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    _response = client.GetStringAsync(url).Result;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    //jo_mai
                }
            }
            if (!string.IsNullOrWhiteSpace(_response))
            {
                MessageResponse response = null;
                try
                {
                    response = JsonConvert.DeserializeObject<MessageResponse>(_response) as MessageResponse;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    //jo_mai
                }
                //If Not Ok Then Return Not OK As Exception
                if (response != null && response.Ok)
                {
                    return response.Result;
                }
            }
            return null;
        }

        public Message EditMessageCaption(int chat_id = 0, int message_id = 0, string inline_message_id = "", string caption = "", string reply_markup = null)
        {
            string url = $"{BASE_URL}{API_TOKEN}/editMessageCaption?chat_id={chat_id}&message_id={message_id}&inline_message_id={inline_message_id}&caption={caption}&reply_markup={reply_markup}";
            string _response = string.Empty;
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    _response = client.GetStringAsync(url).Result;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    //jo_mai
                }
            }
            if (!string.IsNullOrWhiteSpace(_response))
            {
                MessageResponse response = null;
                try
                {
                    response = JsonConvert.DeserializeObject<MessageResponse>(_response) as MessageResponse;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    //jo_mai
                }
                //If Not Ok Then Return Not OK As Exception
                if (response != null && response.Ok)
                {
                    return response.Result;
                }
            }
            return null;
        }

        public Message EditMessageReplyMarkup(int chat_id = 0, int message_id = 0, string inline_message_id = null, string reply_markup = null)
        {
            string url = $"{BASE_URL}{API_TOKEN}/editMessageReplyMarkup?chat_id={chat_id}&message_id={message_id}&inline_message_id={inline_message_id}&reply_markup={reply_markup}";
            string _response = string.Empty;
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    _response = client.GetStringAsync(url).Result;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    //jo_mai
                }
            }
            if (!string.IsNullOrWhiteSpace(_response))
            {
                MessageResponse response = null;
                try
                {
                    response = JsonConvert.DeserializeObject<MessageResponse>(_response) as MessageResponse;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    //jo_mai
                }
                //If Not Ok Then Return Not OK As Exception
                if (response != null && response.Ok)
                {
                    return response.Result;
                }
            }
            return null;
        }

        public bool AnswerInlineQuery(string inline_query_id, InlineQueryResult[] results, int cache_time = 300, bool is_personal = false, string next_offset = null, string switch_pm_text = null, string switch_pm_parameter = null)
        {
            return true;
        }

        //InlineQueryStuff
        ///Payments

        public Message SendInvoice(int chat_id, string title, string payload, string provider_taken, string start_parameter, string currency, LabeledPrice[] prices, string photo_url = null, string description = null, int photo_size = -1, int photo_width = -1, int photo_height = -1, bool need_name = false, bool need_phone_number = false, bool need_email = false, bool need_shipping_address = false, bool is_flexible = false, bool disable_notification = false, int reply_to_message = -1, string reply_markup = null )
        {
            return null;
        }

        public bool AnswerShippingQuery(string shipping_query_id, bool ok, ShippingOption[] shipping_options = null, string error_message = null)
        {
            return false;
        }

        public bool AnswerPreCheckoutQuery(string pre_checkout_query_id, bool ok, string error_message)
        {
            return false;
        }
        
        ///Games
        public Message SendGame(int chat_id, string game_short_name, bool disable_notification = false, int reply_to_message_id = 0, InlineKeyboardMarkup reply_markup = null)
        {
            return null;
        }

        public Message SetGameScore(int user_id, int score, bool force = true, bool disable_edit_message = false, int chat_id = 0, int message_id = 0, string inline_message_id = null)
        {
            return null;
        }

        public GameHighScore[] GetGameHighScores(int user_id, int chat_id = 0, int message_id = 0, string inline_message_id = null)
        {
            return null;
        }

    }



}