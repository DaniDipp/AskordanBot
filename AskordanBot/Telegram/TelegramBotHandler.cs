﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskordanBot.Telegram
{
    class TelegramBotHandler
    {
        public static bool Running { get; private set; }
        public static TelegramBot Bot;

        /// <summary>
        /// This method starts the Bot. The loop-method will be called until the Stop() method is called.
        /// </summary>
        public static void Start()
        {
            Running = true;
            Bot = new TelegramBot();
            update();
        }

        /// <summary>
        /// This method calls the loop-method one time
        /// </summary>
        public static void UpdateOnce()
        {
            oneUpdate();
            Running = false;
        }

        /// <summary>
        /// This method prevents the bot to update again. The current update will be finished, however.
        /// </summary>
        public static void Stop()
        {
            Running = false;
        }

        private static void update()
        {
            while (Running)
            {
                oneUpdate();
            }
        }

        private static void oneUpdate()
        {
            //do the update here
            Bot.Update();
        }
    }
}
